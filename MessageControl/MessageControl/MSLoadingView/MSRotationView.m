//
//  MSRotationView.m
//  MessageControl
//
//  Created by imqiuhang on 15/3/16.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import "MSRotationView.h"
#import "UIView+RotateViewCtg.h"
#import "UIView+QHUIViewCtg.h"
@implementation MSRotationView
- (id)initWithFrame:(CGRect)frame {
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor=[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:0.8];
        [self initView];
    }
    return self;
}

- (void)initView {
    _headImageView = [[UIImageView alloc] init];
    _headImageView.backgroundColor = [UIColor clearColor];
    _headImageView.center=CGPointMake(self.width/2, self.height/2);
    _headImageView.backgroundColor=[UIColor clearColor];
    _headImageView.frame = CGRectMake(0, 0, 100, 100);
    _headImageView.image=[UIImage imageNamed:@"pic1"];
    _headImageView.center=CGPointMake(self.width/2, self.height/2);
    [self addSubview:_headImageView];
    
    //[backImageView addSubview:_headImageView];
    [_headImageView rotate360WithDuration:0.9
                              repeatCount:10000];
    _headImageView.animationDuration = 4.5;
    _headImageView.animationImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"pic1"],
                                      [UIImage imageNamed:@"pic2"],[UIImage imageNamed:@"pic3"],
                                      [UIImage imageNamed:@"pic4"],[UIImage imageNamed:@"pic5"],
                                      nil];
    _headImageView.animationRepeatCount = 0;
    [self performSelector:@selector(startR) withObject:nil afterDelay:0.45f];
 

}
- (void)startR {
    [_headImageView startAnimating];
}

@end
