//
//  UIView+RotateViewCtg.m
//  MessageControl
//
//  Created by imqiuhang on 15/3/16.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import "UIView+RotateViewCtg.h"

@implementation UIView (RotateViewCtg)
- (void)rotate360WithDuration:(CGFloat)aDuration repeatCount:(CGFloat)aRepeatCount {
    CAKeyframeAnimation *theAnimation = [CAKeyframeAnimation animation];
    theAnimation.values = @[
                            [NSValue valueWithCATransform3D:CATransform3DMakeRotation(0, 0,1,0)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI, 0,1,0)],
                            ];
    theAnimation.cumulative = YES;
    theAnimation.duration = aDuration;
    theAnimation.repeatCount = aRepeatCount;
    theAnimation.removedOnCompletion = NO;
   // theAnimation.timingFunctions =
//    [NSArray arrayWithObjects:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn],
//     
//     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut],
//     nil
//     ];
    
    [self.layer addAnimation:theAnimation forKey:@"transform"];
}
@end
