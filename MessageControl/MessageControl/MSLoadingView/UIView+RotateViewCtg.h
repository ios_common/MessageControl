//
//  UIView+RotateViewCtg.h
//  MessageControl
//
//  Created by imqiuhang on 15/3/16.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RotateViewCtg)
- (void)rotate360WithDuration:(CGFloat)aDuration repeatCount:(CGFloat)aRepeatCount;
@end
