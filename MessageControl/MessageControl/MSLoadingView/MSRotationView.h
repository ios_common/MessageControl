//
//  MSRotationView.h
//  MessageControl
//
//  Created by imqiuhang on 15/3/16.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSRotationView : UIView
@property (nonatomic,retain) UIImageView *headImageView;
@end
