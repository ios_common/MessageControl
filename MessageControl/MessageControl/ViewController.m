//
//  ViewController.m
//  MessageControl
//
//  Created by imqiuhang on 15/3/15.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import "ViewController.h"
#import "MessageManager.h"
#import "MsgListViewController.h"

@interface ViewController ()<MessageManagerDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[Constant instance] loadFromPlist];
    
    [MessageManager sharedinstance].delegate=self;
    
    
      [[MessageManager sharedinstance] setMsgShowTypeWithGroupId:10000 andType:[MessageShowType msgShowTypeMake:@"评论" andImageName:@"pic1" andMsgDetailStyle:MessageDetailStyleImage]];
    [[MessageManager sharedinstance] setMsgShowTypeWithGroupId:10001 andType:[MessageShowType msgShowTypeMake:@"订阅活动" andImageName:@"pic2" andMsgDetailStyle:MessageDetailStyleNone]];
    [[MessageManager sharedinstance] setMsgShowTypeWithGroupId:10002 andType:[MessageShowType msgShowTypeMake:@"感兴趣" andImageName:@"pic3" andMsgDetailStyle:MessageDetailStyleBtn]];
    [[MessageManager sharedinstance] setMsgShowTypeWithGroupId:10003 andType:[MessageShowType msgShowTypeMake:@"小见见说" andImageName:@"pic3" andMsgDetailStyle:MessageDetailStyleJump]];
    [[MessageManager sharedinstance] setMsgShowTypeWithGroupId:easeMessageGroupId andType:[MessageShowType msgShowTypeMake:@"" andImageName:@"" andMsgDetailStyle:MessageDetailStyleNone]];
    
    [[MessageManager sharedinstance]setFirstGroupId:10001 andLastGroupId:10003];
    
    

    self.title=@"demo";
    UIButton *btn =[[UIButton alloc] initWithFrame:CGRectMake(100, 100, 100, 30)];
    
    [btn setBackgroundColor:[UIColor greenColor]];
    [btn setTitle:@"进入消息页" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(push) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
}


- (void)push {
    MsgListViewController *msgListViewController =[[MsgListViewController alloc] init];
    [self.navigationController pushViewController:msgListViewController animated:YES];
    
    
    
    
   [self performSelector:@selector(showLoading) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(hideLoading) withObject:nil afterDelay:5];
    [self performSelector:@selector(addTalk) withObject:nil afterDelay:10];
    
    
}

- (void)showLoading {
     [[MessageManager sharedinstance] showLoadingView];
}

- (void)addTalk {
    MessageInfo *talk1=[MessageInfo MsgInfoMakeWithGroupId:easeMessageGroupId andMessageId:763126 andMessageTitle:@"桃子" andMessageTime:@"今天下午" andMsgSubTitle:@"茶的味道不错哦" andSenderHeadImgUrl:@"pic1" andRightImageUrl:nil andRightBtnTitle:nil];
    
        MessageInfo *talk2=[MessageInfo MsgInfoMakeWithGroupId:easeMessageGroupId andMessageId:763336 andMessageTitle:@"桃子2" andMessageTime:@"今天下午" andMsgSubTitle:@"茶的味道不错哦" andSenderHeadImgUrl:@"pic1" andRightImageUrl:nil andRightBtnTitle:nil];
    NSMutableArray *mArr=[[NSMutableArray alloc] initWithObjects:talk1, talk2,nil];
    [[MessageManager sharedinstance] addMessage:mArr];
    
    MSLog(@"%@",[Constant instance].messageData);
    [self performSelector:@selector(addTalk) withObject:nil afterDelay:2];
}
- (void)hideLoading{
    
    NSArray * testData=@[@{@"groupId":@"10000",@"messageId":@"200001",@"messageTitle":@"小桃4子",@"subTitle":@"茶的味道不错~~~!",@"time":@"今天下午",@"senderImge":@"pic1",@"rightImage":@"pic1"},
                         
                         @{@"groupId":@"10000",@"messageId":@"200002",@"messageTitle":@"小桃子1",@"subTitle":@"茶的味道不错!!!",@"time":@"今天下午",@"senderImge":@"pic2",@"rightImage":@"pic1"},
                         
                         @{@"groupId":@"10001",@"messageId":@"200003",@"messageTitle":@"小桃子2",@"subTitle":@"茶的味道不错!--",@"time":@"今天下午",@"senderImge":@"pic3",@"rightImage":@"pic1"},
                         
                         @{@"groupId":@"10000",@"messageId":@"200004",@"messageTitle":@"小桃子3",@"subTitle":@"茶的味道不错!^^",@"time":@"今天下午",@"senderImge":@"pic4",@"rightImage":@"pic1"},
                         @{@"groupId":@"10002",@"messageId":@"200001",@"messageTitle":@"小桃子3",@"subTitle":@"茶的味道不错!^^",@"time":@"今天下午",@"senderImge":@"pic4",@"rightImage":@"pic1"},
                         @{@"groupId":@"10003",@"messageId":@"200001"}];
    
    
    NSMutableArray *testMessages=[[NSMutableArray alloc] init];
    for(NSDictionary *dic in testData) {
        if (dic) {
            [testMessages addObject:[MessageInfo MsgInfoMakeWithGroupId:[dic[@"groupId"] intValue] andMessageId:[dic[@"messageId"] intValue] andMessageTitle:dic[@"messageTitle"] andMessageTime:dic[@"time"] andMsgSubTitle:dic[@"subTitle"] andSenderHeadImgUrl:dic[@"senderImge"] andRightImageUrl:dic[@"rightImage"] andRightBtnTitle:@"私聊"]];
        }
    }
    

    [[MessageManager sharedinstance] hideLoadingView];
     [[MessageManager sharedinstance] addMessage:testMessages];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)didSelectMessage:(int)groupId andMessageId:(int)MessageId {
    NSLog(@"点击了:groupId%i,,,MessageId%i",groupId,MessageId);
}

@end
