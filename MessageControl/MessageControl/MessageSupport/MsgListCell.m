//
//  MsgListCell.m
//  MessageControl
//
//  Created by imqiuhang on 15/3/16.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import "MsgListCell.h"

@implementation UITableView(MsgListCell)

-(MsgListCell*)MsgListCell{
    
    static NSString *CellIdentifier = @"MsgListCell";
    MsgListCell *cell = (MsgListCell *)[self dequeueReusableCellWithIdentifier:CellIdentifier];
    if (nil == cell) {
        cell = [[MsgListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle  = UITableViewCellSelectionStyleNone;
        cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}
@end

@implementation MsgListCell
{
    UIImageView *leftImageView;
    UILabel     *titleLable;
    UIView      *lineView;
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}



- (void)setInfo:(int)groupId {
    [self initView];
    
    MessageShowType *type=[[MessageManager sharedinstance] getMsgTypeWithGroupId:groupId];
    
    //[leftImageView setImage:[UIImage imageNamed:type.imageName]];
    titleLable.text=type.title;
    if ([Constant instance].messageUnRead[[NSString stringWithFormat:@"%i",groupId]]) {
        if ([[Constant instance].messageUnRead[[NSString stringWithFormat:@"%i",groupId]] intValue]>0) {
            UIView * unReadSignView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
            unReadSignView.backgroundColor=[UIColor redColor];
            unReadSignView.layer.cornerRadius=unReadSignView.height/2;
            unReadSignView.centerX=leftImageView.right;
            unReadSignView.top=leftImageView.top;
            [self addSubview:unReadSignView];
        }
    }
    
}



- (void)initView {
    [self removeAllSubviews];
    leftImageView        = [[UIImageView alloc] init];
    titleLable           = [[UILabel alloc] init];
    lineView=[[UIView alloc] init];

    leftImageView.frame              = CGRectMake(15, 0, self.height-20, self.height-20);
    leftImageView.centerY            = self.height/2;
    leftImageView.layer.cornerRadius = leftImageView.height/2;
    leftImageView.layer.masksToBounds=YES;
    leftImageView.backgroundColor    = [UIColor colorWithRed:20/255.0 green:155/255.0 blue:213/255.0 alpha:1.0];
    titleLable.font                  = [UIFont systemFontOfSize:16];
    titleLable.textColor             = [UIColor blackColor];
    titleLable.frame                 = CGRectMake(0, 0, self.width-100, 40);
    titleLable.left                  = leftImageView.right+15;
    titleLable.centerY               = self.height/2;
    lineView.backgroundColor         = [UIColor groupTableViewBackgroundColor];
    lineView.frame                   = CGRectMake(0, 0, self.width, 0.5f);
    lineView.bottom                  = self.height;
    
    [self addSubview:leftImageView];
    [self addSubview:titleLable];
    [self addSubview:lineView];
}


@end






