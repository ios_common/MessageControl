//
//  MessageManager.h
//  MessageControlDemo
//
//  Created by imqiuhang on 15/3/13.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "MessageInfo.h"
#import "UIView+QHUIViewCtg.h"
#import "MessageDataSave.h"
#import "UIView+RotateViewCtg.h"


@protocol MessageManagerDelegate <NSObject>
- (void)didSelectMessage:(int)groupId andMessageId:(int)MessageId;
@end

@interface MessageManager : NSObject
{
@private
    NSArray *listData;
    NSArray *detailData;
}
@property(nonatomic,weak)id <MessageManagerDelegate>delegate;

- (void)addMessage:(NSMutableArray*)data;
- (void)setMsgShowTypeWithGroupId:(int)groupId andType:(MessageShowType *)atype;
- (void)setFirstGroupId:(int )aFirstGroupId andLastGroupId:(int)lastGroupId;
- (void)showLoadingView;
- (void)hideLoadingView;

//@private 不需要关注的方法
- (void)setGroupMsgUnReadCount:(int)groupId andStatus:(int)unReadCount;
- (void)deleteMessage:(NSArray*)data notify:(BOOL)needNotify;
- (NSArray *)getRealData;
- (NSArray *)getEaseMessages;
- (NSArray *)getMessageListWithGroupId:(int)groupId;
- (MessageShowType *)getMsgTypeWithGroupId:(int)groupId;
- (void)reloadData;
+ (BOOL)isHaveValue:(NSMutableArray *)dic andVaule:(id)aValue;

+ (instancetype)sharedinstance;

@end





