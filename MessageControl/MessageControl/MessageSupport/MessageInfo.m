//
//  MessageInfo.m
//  MessageControl
//
//  Created by imqiuhang on 15/3/15.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import "MessageInfo.h"

@implementation MessageInfo

+ (instancetype)MsgInfoMakeWithGroupId:(int)aGId andMessageId:(int)aMId andMessageTitle:(NSString *)aMsgTitle andMessageTime:(NSString *)aMsgTime andMsgSubTitle:(NSString *)aMegSubTitle andSenderHeadImgUrl:(NSString *)aSdImageUrl andRightImageUrl:(NSString *)aRightImgUrl andRightBtnTitle:(NSString *)aRBtnTitle {
    
    MessageInfo *messageInfo = [[MessageInfo alloc] init];
    
    messageInfo.groupId            = aGId;
    messageInfo.messageId          = aMId;
    messageInfo.messageTitle       = aMsgTitle;
    messageInfo.messageTime        = aMsgTime;
    messageInfo.msgSubTitle        = aMegSubTitle;
    messageInfo.senderHeadImageUrl = aSdImageUrl;
    messageInfo.rightImageUrl      = aRightImgUrl;
    messageInfo.rightBtnTitle      = aRBtnTitle;
    
    return messageInfo;
}

@end

@implementation MessageShowType

+(instancetype)msgShowTypeMake:(NSString *)aTitle andImageName:(NSString *)aImageName andMsgDetailStyle:(MessageDetailStyle)aStyle {
    MessageShowType *messageShowType=[[MessageShowType alloc] init];
    messageShowType.title          = aTitle;
    messageShowType.imageName      = aImageName;
    messageShowType.msgDetailStyle = aStyle;
    return messageShowType;
    
}

@end