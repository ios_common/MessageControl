//
//  MessageDetailCell.h
//  MessageControl
//
//  Created by imqiuhang on 15/3/16.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageManager.h"
@interface MessageDetailCell : UITableViewCell
- (void)setInfo:(MessageInfo *)info;
@end

@interface UITableView (MessageDetailCell)
- (MessageDetailCell*)messageDetailCell;
@end