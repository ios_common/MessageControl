//
//  Constant.m
//  xiaomiPus_test
//
//  Created by imqiuhang on 15/3/12.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import "Constant.h"

@implementation Constant

//需要线程同步的数据
@synthesize messageSetting,messageData,messageUnRead,firstGroupId,lastGroupId;

- (id)init{
    if (self=[super init]) {

        self.messageSetting = [[NSDictionary alloc] init];
        self.messageUnRead  = [[NSDictionary alloc] init];
        self.messageData    = [[NSArray alloc] init];
        self.firstGroupId   = -999;
        self.lastGroupId    = -998;
        
        return self;
    }
    return nil;
}

+ (Constant *)instance {
    static Constant *_instance = nil;
    static dispatch_once_t once_token;
    dispatch_once(&once_token, ^{
        _instance = [[Constant alloc] init];
    });

    return _instance;
}

- (void)loadFromPlist {
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *docfilePath = [basePath stringByAppendingPathComponent:@"MessageInfo.plist"];
    NSMutableDictionary *plistdict = [NSMutableDictionary dictionaryWithContentsOfFile:docfilePath];
    
    if (plistdict[@"messageData"]) {
        self.messageData = plistdict[@"messageData"];
    }
    if (plistdict[@"messageUnRead"]) {
        self.messageUnRead = plistdict[@"messageUnRead"];
    }
    if (plistdict[@"messageSetting"]) {
        self.messageSetting = plistdict[@"messageSetting"];
    }
    if (plistdict[@"firstGroupId"]) {
        self.firstGroupId = [plistdict[@"firstGroupId"] intValue];
    }
    if (plistdict[@"lastGroupId"]) {
        self.lastGroupId = [plistdict[@"lastGroupId"] intValue];
    }
    
    
}

- (void)saveToPlist {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"MessageInfo" ofType:@"plist"];
    NSMutableDictionary *plistdict = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];

    [plistdict setObject:self.messageData forKey:@"messageData"];
    [plistdict setObject:self.messageUnRead forKey:@"messageUnRead"];
    [plistdict setObject:self.messageSetting forKey:@"messageSetting"];
    [plistdict setObject:@(self.firstGroupId) forKey:@"firstGroupId"];
    [plistdict setObject:@(self.lastGroupId) forKey:@"lastGroupId"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *docfilePath = [basePath stringByAppendingPathComponent:@"MessageInfo.plist"];
    [plistdict writeToFile:docfilePath atomically:YES];
    
    
    
}


@end
