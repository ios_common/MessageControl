//
//  Constant.h
//  xiaomiPus_test
//
//  Created by imqiuhang on 15/3/12.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef DEBUG
#define MSLog(fmt,...) NSLog((@"\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n[行号]%d\n" "[函数名]%s\n" "[日志]"fmt"\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"),__LINE__,__FUNCTION__,##__VA_ARGS__);
#else
#define MSLog(fmt,...);
#endif

#define msScreenWidth    [[UIScreen mainScreen] bounds].size.width
#define msScreenHeight    [[UIScreen mainScreen] bounds].size.height-20

@interface Constant : NSObject

@property (nonatomic,strong) NSArray  *messageData;   //消息集合
@property (nonatomic,strong) NSDictionary  *messageSetting;//消息类型设置集合
@property (nonatomic,strong) NSDictionary  *messageUnRead; //未读消息ID集合
@property (nonatomic,assign) int firstGroupId;
@property (nonatomic,assign) int lastGroupId;

- (void)saveToPlist;
- (void)loadFromPlist;

/**
 *  至少IOS 5.0使用单例
 *
 *  @return //返回instancetype单例
 */
+ (Constant *)instance;

@end
