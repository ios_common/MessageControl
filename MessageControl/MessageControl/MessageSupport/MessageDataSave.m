//
//  MessageDataSave.m
//  MessageControl
//
//  Created by imqiuhang on 15/3/15.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import "MessageDataSave.h"

@implementation MessageDataSave

+ (NSDictionary *)getMsgInfoDicWithObject:(MessageInfo *)aInfo {

    NSDictionary *curInfo=@{@"groupId":@(aInfo.groupId)?@(aInfo.groupId):@(-100900),
                            @"messageId":@(aInfo.messageId)?@(aInfo.messageId):@(-10900),
                            @"messageTitle":aInfo.messageTitle?aInfo.messageTitle:@"",
                            @"messageTime":aInfo.messageTime?aInfo.messageTime:@"",
                            @"msgSubTitle":aInfo.msgSubTitle?aInfo.msgSubTitle:@"",
                            @"senderHeadImageUrl":aInfo.senderHeadImageUrl?aInfo.senderHeadImageUrl:@"",
                            @"rightImageUrl":aInfo.rightImageUrl?aInfo.rightImageUrl:@"",
                            @"rightBtnTitle":aInfo.rightBtnTitle?aInfo.rightBtnTitle:@""};
    return curInfo;

}


+ (NSDictionary *)getMsgTypeDicWithObject:(MessageShowType *)aType {
    NSDictionary *curtype=@{@"title":aType.title?aType.title:@"",
                            @"imageName":aType.imageName?aType.imageName:@"",
                            @"msgDetailStyle":@(aType.msgDetailStyle)};
    return  curtype;
}

+ (MessageInfo *)getMsgInfoWithDic:(NSDictionary *)aDic {
    MessageInfo *curInfo = [MessageInfo MsgInfoMakeWithGroupId:[aDic[@"groupId"] intValue] andMessageId:[aDic[@"messageId"] intValue] andMessageTitle:aDic[@"messageTitle"] andMessageTime:aDic[@"messageTime"] andMsgSubTitle:aDic[@"msgSubTitle"] andSenderHeadImgUrl:aDic[@"senderHeadImageUrl"] andRightImageUrl:aDic[@"rightImageUrl"] andRightBtnTitle:aDic[@"rightBtnTitle"]];
    return curInfo;
}

+ (MessageShowType *)getMsgTypeWithDic:(NSDictionary *)aDic {
    MessageShowType *curtype=[MessageShowType msgShowTypeMake:aDic[@"title"] andImageName:aDic[@"imageName"] andMsgDetailStyle:[aDic[@"msgDetailStyle"] intValue]];
    return curtype;
}

@end
