//
//  MessageDataSave.h
//  MessageControl
//
//  Created by imqiuhang on 15/3/15.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageManager.h"
@interface MessageDataSave : NSObject

+ (NSDictionary *)getMsgInfoDicWithObject:(MessageInfo*)aInfo;
+ (NSDictionary *)getMsgTypeDicWithObject:(MessageShowType *)aType;

+ (MessageInfo  *)getMsgInfoWithDic:(NSDictionary *)aDic;
+ (MessageShowType *)getMsgTypeWithDic:(NSDictionary *)aDic;

@end
