//
//  MessageManager.m
//  MessageControlDemo
//
//  Created by imqiuhang on 15/3/13.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import "MessageManager.h"

@implementation MessageManager

+ (instancetype)sharedinstance {
    static MessageManager *_instance = nil;
    static dispatch_once_t once_token;
    dispatch_once(&once_token, ^{
        _instance = [[MessageManager alloc] init];
    });
    
    return _instance;
}


- (void)addMessage:(NSMutableArray *)data {
    if (!data||[data count]==0) {
        return;
    }
    
    NSMutableArray *msgArr=[[Constant instance].messageData mutableCopy];
    
    for (int i=0; i<data.count;i++) {
        MessageInfo *info =data[i];
        if (info.groupId==easeMessageGroupId) {
            [self setGroupMsgUnReadCount:info.messageId andStatus:1];
        }
        if ([self isHaveInfo:msgArr andInfo:info]>=0) {
            //相当于是一条重复消息,保险起见 替换了原来的消息,未读数不变
            msgArr[[self isHaveInfo:msgArr andInfo:info]]=[MessageDataSave getMsgInfoDicWithObject:info];
        }else {
            [msgArr addObject:[MessageDataSave getMsgInfoDicWithObject:info]];
            //未读数加1
             [self setGroupMsgUnReadCount:info.groupId andStatus:1];
            MSLog(@"--------%@",info);
        }
    }
    
    [Constant instance].messageData=[msgArr copy];
    
    [[Constant instance] saveToPlist];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:didChangeMessageNotify object:nil userInfo:nil];

    
}

- (void)deleteMessage:(NSArray *)data notify:(BOOL)needNotify{
    if (!data||[data count]==0) {
        return;
    }
    
    NSMutableArray *msgArr=[[Constant instance].messageData mutableCopy];
    for (int i=0; i<data.count;i++) {
        MessageInfo *info =data[i];
        if (info.groupId==easeMessageGroupId) {
            [self setGroupMsgUnReadCount:info.messageId andStatus:0];
        }
        if ([self isHaveInfo:msgArr andInfo:info]>=0) {
            [msgArr removeObjectAtIndex:[self isHaveInfo:msgArr andInfo:info]];
        }else {
            MSLog(@"删除消息失败,不存在消息\n:%@",info);
        }
    }
    
    [Constant instance].messageData=[msgArr copy];
     [[Constant instance] saveToPlist];
    if (needNotify) {
        [[NSNotificationCenter defaultCenter] postNotificationName:didChangeMessageNotify object:nil userInfo:nil];
    }
    
}


- (int) isHaveInfo :(NSMutableArray*)aArr andInfo :(MessageInfo*)aInfo {
    for(int i=0;i<aArr.count;i++) {
        MessageInfo *info =[MessageDataSave getMsgInfoWithDic:aArr[i]];
        if (info.messageId==aInfo.messageId&&info.groupId==aInfo.groupId) {
            return i;
        }
    }
    return -1;
}


- (NSArray *)getRealData {
    NSMutableArray *curDataArr=[[NSMutableArray alloc] init];
    NSMutableArray *msgArr=[[Constant instance].messageData mutableCopy];
    for(NSDictionary *curInfo in msgArr) {
        MessageInfo *info =[MessageDataSave getMsgInfoWithDic:curInfo];
        if ([MessageManager isHaveValue:curDataArr andVaule:@(info.groupId)]) {
            
        }else {
            if ([[Constant instance].messageSetting objectForKey:[NSString stringWithFormat:@"%i",info.groupId]]) {
                
                if (info.groupId!=easeMessageGroupId) {
                    [curDataArr addObject:@(info.groupId)];
                }
                
            }
            
        }
    }
    for (int i=0; i<curDataArr.count; i++) {
        if ([curDataArr[i] intValue]==[Constant instance].firstGroupId) {
            [curDataArr exchangeObjectAtIndex:0 withObjectAtIndex:i];
            break;
        }
    }
    for (int i=0; i<curDataArr.count; i++) {
        if ([curDataArr[i] intValue]==[Constant instance].lastGroupId) {
            [curDataArr exchangeObjectAtIndex:(curDataArr.count-1) withObjectAtIndex:i];
            break;
        }
    }
    
    if (![MessageManager isHaveValue:curDataArr andVaule:@([Constant instance].firstGroupId) ]) {
        [curDataArr insertObject:@([Constant instance].firstGroupId) atIndex:0];
    }
    
    if (![MessageManager isHaveValue:curDataArr andVaule:@([Constant instance].lastGroupId) ]) {
        [curDataArr insertObject:@([Constant instance].lastGroupId) atIndex:curDataArr.count];
    }
        return [curDataArr copy];
    
}

- (NSArray *)getEaseMessages {
    NSMutableArray *curDataArr=[[NSMutableArray alloc] init];
    NSMutableArray *msgArr=[[Constant instance].messageData mutableCopy];
    for(NSDictionary *curInfo in msgArr) {
        MessageInfo *info =[MessageDataSave getMsgInfoWithDic:curInfo];
        if (info.groupId==easeMessageGroupId) {
            [curDataArr addObject:info];
        }
    }
    return [curDataArr copy];
}

- (NSArray *)getMessageListWithGroupId:(int)groupId {
    NSMutableArray *msgArr=[[Constant instance].messageData mutableCopy];
    NSMutableArray *curDataArr=[[NSMutableArray alloc] init];
    for(NSDictionary *curDic in msgArr ) {
        MessageInfo *info =[MessageDataSave getMsgInfoWithDic:curDic];
        if (info.groupId==groupId) {
            [curDataArr addObject:info];
        }
    }
    return [curDataArr copy];
}

- (void)setMsgShowTypeWithGroupId:(int)groupId andType:(MessageShowType *)atype {
    
    NSMutableDictionary *curDic=[[Constant instance].messageSetting mutableCopy];
    [curDic setObject:[MessageDataSave getMsgTypeDicWithObject:atype] forKey:[NSString stringWithFormat:@"%i",groupId]];
    [Constant instance].messageSetting=[curDic copy];
    [[Constant instance] saveToPlist];
}

- (void)setFirstGroupId:(int)aFirstGroupId andLastGroupId:(int)lastGroupId {
    [Constant instance].firstGroupId=aFirstGroupId;
    [Constant instance].lastGroupId=lastGroupId;
    [[Constant instance] saveToPlist];
    
}

- (MessageShowType *)getMsgTypeWithGroupId:(int)groupId {
    
        MessageShowType *type=[MessageDataSave getMsgTypeWithDic:[Constant instance].messageSetting[[NSString stringWithFormat:@"%i",groupId]]];
    return type?type:[MessageShowType msgShowTypeMake:@"" andImageName:@"" andMsgDetailStyle:MessageDetailStyleJump];
}

- (void)reloadData {
    [[NSNotificationCenter defaultCenter] postNotificationName:didChangeMessageNotify object:nil userInfo:nil];
}

- (void)showLoadingView {
    [[NSNotificationCenter defaultCenter] postNotificationName:changeLoadingViewNotify object:@(YES) userInfo:nil];
}

- (void)hideLoadingView {
       [[NSNotificationCenter defaultCenter] postNotificationName:changeLoadingViewNotify object:@(NO) userInfo:nil];
}

+ (BOOL)isHaveValue:(NSMutableArray *)dic andVaule:(id)aValue {
    for(id value in dic) {

        if ([value isEqual:aValue]) {
            
            return YES;
        }
    }
    return NO;
}

- (void)setGroupMsgUnReadCount:(int)groupId andStatus:(int)unReadCount {
    NSMutableDictionary *curDic=[[Constant instance].messageUnRead mutableCopy];
    if (unReadCount<=0) {
        [curDic setObject:@(unReadCount) forKey:[NSString stringWithFormat:@"%i",groupId]];
    } else {
        if (curDic[[NSString stringWithFormat:@"%i",groupId]]) {
            int count=[curDic[[NSString stringWithFormat:@"%i",groupId]] intValue];
            [curDic setObject:@(unReadCount+count) forKey:[NSString stringWithFormat:@"%i",groupId]];
        }else {
            
            [curDic setObject:@(unReadCount) forKey:[NSString stringWithFormat:@"%i",groupId]];
        }
    }
    [Constant instance].messageUnRead=[curDic copy];
    [[Constant instance] saveToPlist];
}
@end




