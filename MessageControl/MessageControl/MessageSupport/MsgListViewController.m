//
//  MsgListViewController.m
//  MessageControl
//
//  Created by imqiuhang on 15/3/16.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import "MsgListViewController.h"
#import "MsgListCell.h"
#define msgListCellHeight 65.f
#import "MsgDetailViewController.h"
#import "MSRotationView.h"
#import "MessageDetailCell.h"
@interface MsgListViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation MsgListViewController
{
    UITableView *dataTableView;
    NSArray *data;
    MSRotationView *rotationView;
    NSArray *easeData;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    self.title=@"消息";
    [self initView];
    [self loadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:didChangeMessageNotify object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showOrHideLoadingView:) name:changeLoadingViewNotify object:nil];
 
}


#pragma mark -
#pragma mark loadData
- (void)loadData {
    data=[[MessageManager sharedinstance] getRealData];
    easeData = [[MessageManager sharedinstance] getEaseMessages];
    [dataTableView reloadData];
    
    MSLog(@">>>>>>>>>>>>%@",easeData);
}




#pragma mark -
#pragma mark tableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return easeData?2:1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return data?data.count:0;
    }
    return easeData.count?easeData.count:0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        if (data&&[data count]>0) {
            MsgListCell *cell =[tableView MsgListCell];
            cell.height=msgListCellHeight;
            cell.width=msScreenWidth;
            [cell setInfo:[data[indexPath.row] intValue]];
            return cell;
        }else {
            return nil;
        }

    }else if(indexPath.section==1) {
        MessageDetailCell *cell=[tableView messageDetailCell];
        cell.height=msgListCellHeight;
        cell.width=msScreenWidth;
        [cell setInfo:easeData[indexPath.row]];
        return cell;
    }
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return msgListCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        if (data&&[data count]>0) {
            
            MessageShowType *type = [[MessageManager sharedinstance] getMsgTypeWithGroupId:[data[indexPath.row] intValue]];
            [[MessageManager sharedinstance] setGroupMsgUnReadCount:[data[indexPath.row] intValue] andStatus:0];
            [[MessageManager sharedinstance] reloadData];
            if (type.msgDetailStyle ==MessageDetailStyleBtn||type.msgDetailStyle==MessageDetailStyleImage
                ||type.msgDetailStyle==MessageDetailStyleNone) {
                MsgDetailViewController *msgDetailViewController = [[MsgDetailViewController alloc] init];
                msgDetailViewController .groupId =[data[indexPath.row] intValue];
                [self.navigationController pushViewController:msgDetailViewController animated:YES];
            }
//            [MessageManager sharedinstance].delegate
            if ([[MessageManager sharedinstance].delegate respondsToSelector:@selector(didSelectMessage:andMessageId:)]) {
                [[MessageManager sharedinstance].delegate didSelectMessage:[data[indexPath.row] intValue] andMessageId:-999999];
            }
        }
    }else {
        MessageInfo *info = easeData[indexPath.row];
        [[MessageManager sharedinstance] setGroupMsgUnReadCount:info.messageId andStatus:0];
        [[MessageManager sharedinstance] reloadData];
        if ([[MessageManager sharedinstance].delegate respondsToSelector:@selector(didSelectMessage:andMessageId:)]) {
            [[MessageManager sharedinstance].delegate didSelectMessage:easeMessageGroupId andMessageId:info.messageId];
        }
    }

}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==1) {
        return UITableViewCellEditingStyleDelete ;
    }
    return  UITableViewCellEditingStyleNone;
}


- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==1) {
        if (editingStyle==UITableViewCellEditingStyleDelete) {
            MessageInfo *info=easeData[indexPath.row];
            NSMutableArray *mdic=[easeData mutableCopy];
            [mdic removeObjectAtIndex:indexPath.row];
             [[MessageManager sharedinstance] deleteMessage:@[info] notify:NO];
            easeData=[mdic copy];
            [dataTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
        }
    }
}


- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==1) {
        return @"删除";
    }
    return nil;
}
#pragma mark -
#pragma mark init
- (void)initView {
    dataTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, msScreenWidth, msScreenHeight+20)];
    dataTableView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    dataTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    dataTableView.delegate       = self;
    dataTableView.dataSource     = self;
    [self.view addSubview:dataTableView];
    rotationView=[[MSRotationView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height)];
}

- (void)showOrHideLoadingView:(NSNotification *)aNotify {
    
    if ([aNotify.object boolValue]) {
        [rotationView removeFromSuperview];
        [self.view addSubview:rotationView];
        [self.view bringSubviewToFront:rotationView];
    } else {
        [rotationView removeFromSuperview];
    }
}

- (void)dealloc {
    MSLog(@"%@:dealloc",NSStringFromClass([self class]));
      [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
