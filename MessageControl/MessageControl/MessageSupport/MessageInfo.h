//
//  MessageInfo.h
//  MessageControl
//
//  Created by imqiuhang on 15/3/15.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#define didChangeMessageNotify   @"didChangeMessageNotify"
#define changeLoadingViewNotify  @"changeLoadingViewNotify"
#define messageTalkGrounpId      73456
#define easeMessageGroupId       -99978
//主列表点进去后的detail列表的右边的风格
typedef enum {
    MessageDetailStyleBtn   = 1,//按钮
    MessageDetailStyleImage = 2,//图片
    MessageDetailStyleNone  = 0,//default
    MessageDetailStyleJump  = 3 //跳转类型,如环信消息类型
}MessageDetailStyle;


@interface MessageInfo : NSObject

@property (nonatomic,assign) int      groupId;
@property (nonatomic,assign) int      messageId;
@property (nonatomic,strong) NSString *messageTitle;
@property (nonatomic,strong) NSString *messageTime;
@property (nonatomic,strong) NSString *msgSubTitle;
//todo
@property (nonatomic,strong) NSString *senderHeadImageUrl;
@property (nonatomic,strong) NSString *rightImageUrl;
@property (nonatomic,strong) NSString *rightBtnTitle;

@property (nonatomic,assign) BOOL isEaseMessage;

+ (instancetype)MsgInfoMakeWithGroupId:(int)aGId
                          andMessageId:(int)aMId
                       andMessageTitle:(NSString*)aMsgTitle
                        andMessageTime:(NSString*)aMsgTime
                        andMsgSubTitle:(NSString*)aMegSubTitle
                   andSenderHeadImgUrl:(NSString*)aSdImageUrl
                      andRightImageUrl:(NSString*)aRightImgUrl
                      andRightBtnTitle:(NSString*)aRBtnTitle;

@end


@interface MessageShowType : NSObject

@property (nonatomic,strong)NSString *title;
@property (nonatomic,strong)NSString *imageName;
@property (nonatomic,assign)MessageDetailStyle msgDetailStyle;

+ (instancetype)msgShowTypeMake:(NSString*)aTitle
                   andImageName:(NSString*)aImageName
              andMsgDetailStyle:(MessageDetailStyle)aStyle;
@end


