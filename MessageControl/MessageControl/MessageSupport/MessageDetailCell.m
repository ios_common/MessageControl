//
//  MessageDetailCell.m
//  MessageControl
//
//  Created by imqiuhang on 15/3/16.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import "MessageDetailCell.h"

@implementation UITableView(MessageDetailCell)

- (MessageDetailCell *)messageDetailCell {
    static NSString *CellIdentifier = @"MessageDetailCell";
    MessageDetailCell *cell = (MessageDetailCell *)[self dequeueReusableCellWithIdentifier:CellIdentifier];
    if (nil == cell) {
        cell = [[MessageDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

@end



@implementation MessageDetailCell
{
    UILabel     *titleLable;
    UILabel     *subTitleLable;
    UILabel     *TimeLable;
    UIImageView *leftImageView;
    UIImageView *rightImageView;
    UIButton    *rightBtn;
    UIView      *lineView;
    
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setInfo:(MessageInfo *)info {
    [self initView:info];
}



- (void)initView:(MessageInfo *)info {
    [self removeAllSubviews];
    
    titleLable     = [[UILabel alloc] init];
    subTitleLable  = [[UILabel alloc] init];
    TimeLable      = [[UILabel alloc] init];
    leftImageView  = [[UIImageView alloc] init];
    rightImageView = [[UIImageView alloc] init] ;
    rightBtn       = [[UIButton alloc] init] ;
    lineView       = [[UIView alloc] init] ;
    

    
    TimeLable.frame = CGRectMake(0, 5, 50, 20);
    TimeLable.right = self.width-15;
    TimeLable.text  = info.messageTime;
    TimeLable.font=[UIFont systemFontOfSize:12];
    
    leftImageView.frame              = CGRectMake(15, 0, self.height-20, self.height-20);
    leftImageView.centerY            = self.height/2;
    leftImageView.layer.cornerRadius = leftImageView.height/2;
    leftImageView.backgroundColor    = [UIColor colorWithRed:20/255.0 green:155/255.0 blue:213/255.0 alpha:1.0];
    leftImageView.layer.masksToBounds=YES;
    leftImageView.image=[UIImage imageNamed:info.senderHeadImageUrl];
    
    titleLable =[[UILabel alloc] initWithFrame:CGRectMake(leftImageView.right+15, 5, 0, 30)];
    titleLable.font=[UIFont systemFontOfSize:16];
    titleLable.text=info.messageTitle;
    
    lineView.backgroundColor         = [UIColor groupTableViewBackgroundColor];
    lineView.frame                   = CGRectMake(0, 0, self.width, 0.5f);
    lineView.bottom                  = self.height;

    subTitleLable.frame= CGRectMake(titleLable.left, 0, self.width-titleLable.left-leftImageView.width, 20);
    subTitleLable.bottom=self.height-5;
    subTitleLable.text=info.msgSubTitle;
    subTitleLable.font=[UIFont systemFontOfSize:14];
    
    [self addSubview:titleLable];
    [self addSubview:leftImageView];
    [self addSubview:subTitleLable];
    [self addSubview:lineView];
    [self addSubview:TimeLable];
    
    
    
    MessageShowType *type=[[MessageManager sharedinstance] getMsgTypeWithGroupId:info.groupId];
    
    if (type.msgDetailStyle==MessageDetailStyleImage) {
        rightImageView.frame = CGRectMake(0, 0, self.height-20, self.height-20);
        rightImageView.right = self.width - 15;
        rightImageView.image = [UIImage imageNamed:info.rightImageUrl];
        rightImageView.centerY=self.height/2;
        [self addSubview:rightImageView];
        TimeLable.right = TimeLable.right-rightImageView.width-15;

    }else if(type.msgDetailStyle==MessageDetailStyleBtn) {
        rightBtn.frame = CGRectMake(0, 0, 50, 30);
        rightBtn.right = self.width - 15;
        rightBtn.backgroundColor=[UIColor colorWithRed:20/255.0 green:155/255.0 blue:213/255.0 alpha:1.0];
        [rightBtn setTitle:info.rightBtnTitle forState:UIControlStateNormal];
        TimeLable.right = TimeLable.right-rightBtn.width-15;
        rightBtn.centerY=self.height/2;
        rightBtn.titleLabel.font=[UIFont systemFontOfSize:13];
        [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self addSubview:rightBtn];
    }else if(type.msgDetailStyle==MessageDetailStyleNone) {
        
    }else {
        
    }
    
    
    titleLable.width=self.width-leftImageView.right-2*15-(self.width-TimeLable.left);
    TimeLable.centerY=titleLable.centerY;

    if (info.groupId==easeMessageGroupId) {
        [self countView:info];
    }
    
    

}

- (void)countView:(MessageInfo *)info {
    
    if ([[Constant instance].messageUnRead[[NSString stringWithFormat:@"%i",info.messageId]] intValue]>0) {
        int count=[[Constant instance].messageUnRead[[NSString stringWithFormat:@"%i",info.messageId]] intValue];
        
        UIView *countView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
        countView.backgroundColor=[UIColor redColor];
        countView.layer.cornerRadius=countView.height/2;
        countView.top=5;
        
        UILabel *countLable=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 15, 15)];
        countLable.textColor=[UIColor whiteColor];
        countLable.text=[NSString stringWithFormat:@"%i",count];
        countLable.centerY=countView.height/2.f;
        [countView addSubview:countLable];
        countLable.font=[UIFont systemFontOfSize:12];
         countView.centerX=leftImageView.right;
        [self addSubview:countView];
        if (count<10) {
            
        }else if (count>=10&&count<=99) {
            countView.width = 25;
            countView.left=countView.left-5;
        }else {
            countView.width = 33;
            countLable.text=@"99+";
            countLable.width=25;
            countView.left=countView.left-12;
        }
       
        countLable.centerX=countView.width/2.f;
        countLable.textAlignment=NSTextAlignmentCenter;

    }
}

@end
