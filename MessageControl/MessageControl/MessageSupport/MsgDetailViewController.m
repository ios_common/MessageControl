//
//  MsgDetailViewController.m
//  MessageControl
//
//  Created by imqiuhang on 15/3/16.
//  Copyright (c) 2015年 imqiuhang. All rights reserved.
//

#import "MsgDetailViewController.h"
#import "MessageManager.h"
#import "MessageDetailCell.h"

#define msgDetailCellHeight 65.f
@interface MsgDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *dataTableView;
   
}
@end

@implementation MsgDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    [self loadData];
    [self initView];
}



- (void)loadData {
    self.data=[[MessageManager sharedinstance] getMessageListWithGroupId:self.groupId];
}

#pragma mark -
#pragma mark tableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.data?1:0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data?[self.data count]:0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.data&&[self.data count]>0) {
        MessageDetailCell *cell =[tableView messageDetailCell];
        cell.height=msgDetailCellHeight;
        cell.width=msScreenWidth;
        [cell setInfo:self.data[indexPath.row]];
        return cell;
    }else {
        return nil;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return msgDetailCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.data&&[self.data count]>0) {
      
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return  UITableViewCellEditingStyleDelete;
}


- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle

 forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle==UITableViewCellEditingStyleDelete) {
        MessageInfo *info=self.data[indexPath.row];
        [[MessageManager sharedinstance] deleteMessage:@[info] notify:YES];
        NSMutableArray *mdic=[self.data mutableCopy];
        [mdic removeObjectAtIndex:indexPath.row];
        self.data=[mdic copy];
        [dataTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
    }
    
}


- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}



#pragma mark -
#pragma mark init
- (void)initView {
    dataTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, msScreenWidth, msScreenHeight+20)];
    dataTableView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    dataTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    dataTableView.delegate       = self;
    dataTableView.dataSource     = self;
    [self.view addSubview:dataTableView];
    
    MessageShowType *type =[[MessageManager sharedinstance] getMsgTypeWithGroupId:self.groupId];
    self.title=type.title;
    
    
   UIButton * editeBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    [editeBtn setTitle:@"清空" forState:UIControlStateNormal ];
    [editeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [editeBtn addTarget:self action:@selector(cleanAllData) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem=[[UIBarButtonItem alloc] initWithCustomView:editeBtn];
    self.navigationItem.rightBarButtonItem=rightItem;

    
    
}

- (void)dealloc {
    MSLog(@"%@:dealloc",NSStringFromClass([self class]));
}

- (void)cleanAllData {
    if (self.data) {
        [[MessageManager sharedinstance] deleteMessage:self.data notify:YES];
        self.data=nil;
        [dataTableView deleteSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
    }

//    [dataTableView reloadData];
}

@end
